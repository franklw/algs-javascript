/* eslint-disable no-extend-native */
import { exch } from './Helpers'

Array.prototype.insertSort = function (dirc = 'ASC') {
  let bD = true
  if (dirc !== 'ASC') bD = false
  const n = this.length

  for (let i = 1; i < n; i++) {
    for (let j = i; j > 0 && this[j] < this[j - 1]; j--)
      exch(this, j, j - 1)
  }
  if (!bD) this.reverse()
}

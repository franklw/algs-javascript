/* global test, expect */

var Stack = require('./Stack')

let tStack = new Stack()

test('test Stack', () => {
  tStack = new Stack()
  expect(tStack.isEmpty()).toBe(true)
  expect(tStack.size()).toBe(0)

  tStack.push('t')
  tStack.push('B')
  expect(tStack.isEmpty()).toBe(false)
  expect(tStack.size()).toBe(2)

  tStack.push(1)
  tStack.push(2)
  expect(tStack.isEmpty()).toBe(false)
  expect(tStack.size()).toBe(4)

  expect(tStack.pop()).toBe(2)
  expect(tStack.pop()).toBe(1)
  expect(tStack.isEmpty()).toBe(false)
  expect(tStack.size()).toBe(2)
  expect(tStack.pop()).toBe('B')
  expect(tStack.pop()).toBe('t')
  expect(tStack.isEmpty()).toBe(true)
  expect(tStack.size()).toBe(0)
})

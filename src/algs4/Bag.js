/* eslint-disable space-before-function-paren  */

/**
 *
 *
 * @class Bag
 */
class Bag {
  constructor () {
    this.elems = {}
    this.ct = 0
  }

  add(v) {
    if (!this.elems.hasOwnProperty(v)) this.ct++
    this.elems[v] = true
  }

  // Implement "iterator protocol"
  * all() {
    for (let key of Object.keys(this.elems)) {
      const value = key
      yield value
    }
  }

  isEmpty() {
    return this.ct === 0
  }

  size() {
    return this.ct
  }
}

module.exports = Bag

var Stack = require('./Stack')

const tStack = new Stack()
tStack.push('s')
tStack.push('t')
console.log('isEmpty', tStack.isEmpty())
console.log('size', tStack.size())
tStack.push(1)
tStack.push(2)
console.log('isEmpty', tStack.isEmpty())
console.log('size', tStack.size())

while (!tStack.isEmpty())
  console.log(tStack.pop())

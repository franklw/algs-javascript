/* eslint-disable no-extend-native */

Array.prototype.mergeSort = function (dirc = 'ASC') {
  const aux = []
  let bD = true
  if (dirc !== 'ASC') bD = false

  const sort = (lo, hi) => {
    if (hi <= lo) return

    const mid = lo + Math.floor((hi - lo) / 2)

    sort(lo, mid) // recuresively reduce indeices down to arrays of one item
    sort(mid + 1, hi)

    merge(lo, mid, hi) // merge two arrays into a new array, merge will position items in order as merge happens.
  }

  const merge = (lo, mid, hi) => {
    let left = lo
    let right = mid + 1

    for (let k = lo; k <= hi; k++) aux[k] = this[k]

    for (let dest = lo; dest <= hi; dest++) {
      if (left > mid)
        // boundry condition left pointer has moved past mid pointer
        // There are no more items in the laft hand array
        this[dest] = aux[right++]
      else if (right > hi)
        // boundry condition. right pointer has moved past hi pointer
        // there are no more items in the right had array
        this[dest] = aux[left++]

      else if (aux[right] < aux[left])
        // if right hand item is less than left, write the right hand items first
        this[dest] = aux[right++]

      else
        // else write the lefhand item first
        this[dest] = aux[left++]
    }
  }

  sort(0, this.length - 1)
  if (!bD) {
    this.reverse()
  }
}
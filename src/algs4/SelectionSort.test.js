/* global test, expect */

import { selectSort } from './SelectionSort'
require('./SelectionSortProto')

let ar = Array.from('MERGESORTEXAMPLE')

test('test insertSort', () => {
  ar = Array.from('ME')
  selectSort(ar)
  // console.log(ar.toString())
  expect(ar).toStrictEqual(['E', 'M'])

  ar = Array.from('MERGESORTEXAMPLE')
  selectSort(ar)
  // console.log(ar.toString())
  expect(ar).toStrictEqual(['A', 'E', 'E', 'E', 'E', 'G', 'L', 'M', 'M', 'O', 'P', 'R', 'R', 'S', 'T', 'X'])
  // expect(ar).toStrictEqual(['X', 'T', 'S', 'R', 'R', 'P', 'O', 'M', 'M', 'L', 'G', 'E', 'E', 'E', 'E', 'A'])

  ar = Array.from('MERGESORTEXAMPLE')
  selectSort(ar, 'DESC')
  // console.log(ar.toString())
  // expect(ar).toStrictEqual(['A', 'E', 'E', 'E', 'E', 'G', 'L', 'M', 'M', 'O', 'P', 'R', 'R', 'S', 'T', 'X'])
  expect(ar).toStrictEqual(['X', 'T', 'S', 'R', 'R', 'P', 'O', 'M', 'M', 'L', 'G', 'E', 'E', 'E', 'E', 'A'])

  ar = Array.from('MERGESORTEXAMPLE')
  ar.selectSort()
  // console.log(ar.toString())
  expect(ar).toStrictEqual(['A', 'E', 'E', 'E', 'E', 'G', 'L', 'M', 'M', 'O', 'P', 'R', 'R', 'S', 'T', 'X'])

  ar = Array.from('MERGESORTEXAMPLE')
  ar.selectSort('DESC')
  // console.log(ar.toString())
  expect(ar).toStrictEqual(['X', 'T', 'S', 'R', 'R', 'P', 'O', 'M', 'M', 'L', 'G', 'E', 'E', 'E', 'E', 'A'])

})

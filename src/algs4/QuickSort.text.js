/* global test, expect */

import { quickSort } from './QuickSort'
require('./QuickSortProto')

let ar = Array.from('MERGESORTEXAMPLE')

test('test quickSort', () => {
  ar = Array.from('ME')
  quickSort(ar)
  // console.log(ar.toString())
  expect(ar).toStrictEqual(['E', 'M'])

  ar = Array.from('MERGESORTEXAMPLE')
  quickSort(ar)
  // console.log(ar.toString())
  expect(ar).toStrictEqual(['A', 'E', 'E', 'E', 'E', 'G', 'L', 'M', 'M', 'O', 'P', 'R', 'R', 'S', 'T', 'X'])

  ar = Array.from('MERGESORTEXAMPLE')
  ar.quickSort()
  // console.log(ar.toString())
  expect(ar).toStrictEqual(['A', 'E', 'E', 'E', 'E', 'G', 'L', 'M', 'M', 'O', 'P', 'R', 'R', 'S', 'T', 'X'])

})

/* global test, expect */

var Node = require('./NodeItem')

let tNode = null

test('test Node', () => {
  tNode = new Node()
  expect(tNode.val).toBe(null)
  // expect(tNode.next()).toBe(null)
  tNode.val = 10
  // tNode.item(11)
  expect(tNode.val).toBe(10)
  // expect(tNode.next()).toBe(11)
})

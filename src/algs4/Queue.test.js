/* global test, expect */

var Queue = require('./Queue')

let tQueue = new Queue()

test('test Queue', () => {
  tQueue = new Queue()
  expect(tQueue.isEmpty()).toBe(true)
  expect(tQueue.size()).toBe(0)

  tQueue.enqueue('t')
  tQueue.enqueue('B')
  expect(tQueue.isEmpty()).toBe(false)
  expect(tQueue.size()).toBe(2)

  tQueue.enqueue(1)
  tQueue.enqueue(2)
  expect(tQueue.isEmpty()).toBe(false)
  expect(tQueue.size()).toBe(4)

  expect(tQueue.dequeue()).toBe('t')
  expect(tQueue.dequeue()).toBe('B')
  expect(tQueue.isEmpty()).toBe(false)
  expect(tQueue.size()).toBe(2)

  expect(tQueue.dequeue()).toBe(1)
  expect(tQueue.dequeue()).toBe(2)
  expect(tQueue.isEmpty()).toBe(true)
  expect(tQueue.size()).toBe(0)

  expect(tQueue.dequeue()).toBe(null)
})

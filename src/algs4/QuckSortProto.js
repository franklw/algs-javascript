/* eslint-disable no-extend-native */

import { exch, shuffle } from './Helpers'

Array.prototype.quickSort = function () {
  const sort = (lo, hi) => {
    // base case, pointers have crossed
    // also no sort for sub array of 1
    if (hi <= lo) return

    // partition into a low and high segment, return pivot index
    // places pivot value into array
    // all items less than pivot value on the left
    // all items greater than pivot value on the right
    // pivot value inbetween
    const j = partition(lo, hi)

    sort(lo, j - 1) // REcursively sort low side around the pivot value
    sort(j + 1, hi) // Recursively sort high side
  }

  const partition = (lo, hi) => {
    // partition range of array
    const pivotVal = this[lo] // pivot value always the first element
    let loPtr = lo // low side pointer
    let hiPtr = hi + 1 // high side pointer, + 1 as we will decrement before using

    while (true) {
      while (this[++loPtr] < pivotVal) // while low pointer value is less than pivot v do nothing
        if (loPtr === hiPtr) // if low point has reached the high side point stop this while
          break

      while (pivotVal < this[--hiPtr]) // while v  is less than high side pointer do nothing
        if (hiPtr === lo) // if high side pointer has reached low side start location stop this while
          break

      if (loPtr >= hiPtr) // if lo side pointer has crossed high side pointer stop outer while
        break
      // now the pointers are posititioned.
      // the lo side pointer is a value that is more than the pivot
      // this high side pointer is a value that is less than the pivot
      // exchange will make the swap.
      exch(this, loPtr, hiPtr)

      // move on to find the next pair that need to be swaped
    }

    // move pivot value int position between sorted low side and sorted high side
    // hiPtr will stop moving once all the items on the high side are more than pivot value
    exch(this, lo, hiPtr)

    return hiPtr
  }

  shuffle(this) // Randomize array
  sort(this, 0, this.length - 1) // start sort in place
}

/* global test, expect */
// import largeText from './largeText'
import quick3Way from './Quick3waySort'
require('./Quick3waySortProto')

let ar = Array.from('MERGESORTEXAMPLE')

// ar = Array.from(largeText)
// console.time('quick3Way  dic')
// quick3Way(ar)
// console.timeEnd('quick3Way  dic')

test('test mergeSort', () => {
  ar = Array.from('ME')
  quick3Way(ar)
  // console.log(ar.toString())
  expect(ar).toStrictEqual(['E', 'M'])

  ar = Array.from('MERGESORTEXAMPLE')
  quick3Way(ar)
  // console.log(ar.toString())
  expect(ar).toStrictEqual(['A', 'E', 'E', 'E', 'E', 'G', 'L', 'M', 'M', 'O', 'P', 'R', 'R', 'S', 'T', 'X'])

  ar = Array.from('MERGESORTEXAMPLE')
  ar.quick3Way()
  // console.log(ar.toString())
  expect(ar).toStrictEqual(['A', 'E', 'E', 'E', 'E', 'G', 'L', 'M', 'M', 'O', 'P', 'R', 'R', 'S', 'T', 'X'])

})

/* eslint-disable no-trailing-spaces */

/**
 * 
 * @param {*} a 
 * @param {*} i 
 * @param {*} j 
 */
const exch = (a, i, j) => {
  const t = a[i]
  a[i] = a[j]
  a[j] = t
}

/**
 * 
 * @param {*} array 
 */
const shuffle = (array) => {
  let i = 0
  let j = 0
  let temp = null

  for (i = array.length - 1; i > 0; i -= 1) {
    j = Math.floor(Math.random() * (i + 1))
    temp = array[i]
    array[i] = array[j]
    array[j] = temp
  }
}

/**
 * 
 * @param {*} a 
 * @param {*} b 
 */
const comp = (a, b) => {
  if (a < b) return -1
  else if (a > b) return 1
  else return 0
}

module.exports = { exch, shuffle, comp }

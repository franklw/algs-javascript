/**
 *
 *
 * @class Node
 */
class Node {
  constructor (val = null) {
    this.val = val
    this.next = null
  }

  get val () {
    return this._val
  }

  get next () {
    return this._next
  }

  set val (v) {
    this._val = v
  }

  set next (link) {
    this._next = link
  }
}

module.exports = Node

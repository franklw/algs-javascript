/* global test, expect */
// import largeText from './largeText'
var mergeSort = require('./MergeSort')
require('./MergeSortProto')

let ar = Array.from('MERGESORTEXAMPLE')

// ar = Array.from(largeText)
// console.time('mergeSort  dic')
// mergeSort(ar)
// console.timeEnd('mergeSort  dic')

test('test mergeSort', () => {
  ar = Array.from('ME')
  mergeSort(ar)
  expect(ar).toStrictEqual(['E', 'M'])

  ar = Array.from('MERGESORTEXAMPLE')

  mergeSort(ar, 'DESC')
  expect(ar).toStrictEqual(['X', 'T', 'S', 'R', 'R', 'P', 'O', 'M', 'M', 'L', 'G', 'E', 'E', 'E', 'E', 'A'])

  ar = Array.from('MERGESORTEXAMPLE')
  ar.mergeSort()
  expect(ar).toStrictEqual(['A', 'E', 'E', 'E', 'E', 'G', 'L', 'M', 'M', 'O', 'P', 'R', 'R', 'S', 'T', 'X'])

  ar = Array.from('MERGESORTEXAMPLE')
  ar.mergeSort('DESC')
  expect(ar).toStrictEqual(['X', 'T', 'S', 'R', 'R', 'P', 'O', 'M', 'M', 'L', 'G', 'E', 'E', 'E', 'E', 'A'])

})

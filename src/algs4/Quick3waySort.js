import { exch, comp } from './Helpers'

/**
 * 
 * @param {*} a 
 */
const quick3Way = (a) => {
  const sort = (a, lo, hi) => {
    if (hi <= lo) return // Base case

    const v = a[lo]
    let lt = lo
    let i = lo + 1
    let gt = hi
    
    while (i <= gt) {
      let cmp = comp(a[i], v)

      if (cmp < 0)
        exch(a, lt++, i++)
      else if (cmp > 0)
        exch(a, i, gt--)
      else
        i++
    }
    sort(a, lo, lt - 1)
    sort(a, gt + 1, hi)
  }
  sort(a, 0, a.length - 1)
}

export default quick3Way

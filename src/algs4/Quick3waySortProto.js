/* eslint-disable no-extend-native */

import { exch, comp } from './Helpers'

Array.prototype.quick3Way = function () {
  const sort = (lo, hi) => {
    if (hi <= lo) return // Base case

    const v = this[lo]
    let lt = lo
    let i = lo + 1
    let gt = hi
    
    while (i <= gt) {
      let cmp = comp(this[i], v)

      if (cmp < 0)
        exch(this, lt++, i++)
      else if (cmp > 0)
        exch(this, i, gt--)
      else
        i++
    }
    sort(lo, lt - 1)
    sort(gt + 1, hi)
  }
  sort(0, this.length - 1)
}

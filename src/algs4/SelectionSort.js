/* eslint-disable space-before-function-paren  */
import { exch } from './Helpers'

/**
 * sort min to max
 * algs pp 249
 * @param {*} a
 */
export const selectSort = (a, dirc = 'ASC') => {
  const n = a.length
  let bD = true
  if (dirc !== 'ASC') bD = false

  for (let i = 0; i < n; i++) {
    let min = i
    for (let j = i + 1; j < n; j++) {
      if (a[j] < a[min]) min = j
    }
    exch(a, i, min)
  }
  if (!bD) a = a.reverse()
}

/**
 * sort max to min
 * @param {*} a
 */
export const selectSortMax = (a, dirc = 'ASC') => {
  const n = a.length
  let bD = true
  if (dirc !== 'ASC') bD = false
  let hi = n - 1
  let lo = 0
  let min
  let max

  while (lo >= 0 && lo <= hi) {
    min = lo
    max = hi
    for (let j = lo + 1; j <= hi; j++) {
      if (a[j] < a[min]) min = j
      if (a[j] > a[max]) max = j
    }
    exch(a, lo++, min)
    exch(a, hi--, max)
  }
  if (!bD) a = a.reverse()
}

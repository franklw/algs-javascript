/* global test, expect */

// import largeText from './largeText'
import insertSort from './InsertSort'
require('./InsertSortProto')

let ar = Array.from('INSERTIONSORTEXAMPLE')

// ar = Array.from(largeText)
// console.time('insertSort  largeText')
// insertSort(ar)
// console.timeEnd('insertSort  largeText')

test('test insertSort', () => {
  ar = Array.from('ME')
  insertSort(ar)
  // console.log(ar.toString())
  expect(ar).toStrictEqual(['E', 'M'])

  ar = Array.from('MERGESORTEXAMPLE')

  insertSort(ar)
  // console.log(ar.toString())
  expect(ar).toStrictEqual(['A', 'E', 'E', 'E', 'E', 'G', 'L', 'M', 'M', 'O', 'P', 'R', 'R', 'S', 'T', 'X'])
  // A,E,E,E,I,I,L,M,N,N,O,O,P,R,R,S,S,T,T,X

  ar = Array.from('MERGESORTEXAMPLE')
  ar.insertSort()
  // console.log(ar.toString())
  expect(ar).toStrictEqual(['A', 'E', 'E', 'E', 'E', 'G', 'L', 'M', 'M', 'O', 'P', 'R', 'R', 'S', 'T', 'X'])
  // A,E,E,E,I,I,L,M,N,N,O,O,P,R,R,S,S,T,T,X
})
/* global test, expect */

var Bag = require('./Bag')

let tBag = new Bag()

test('test Bag', () => {
  tBag = new Bag()
  expect(tBag.isEmpty()).toBe(true)
  expect(tBag.size()).toBe(0)
  
  tBag.add('t')
  tBag.add('B')
  expect(tBag.isEmpty()).toBe(false)
  expect(tBag.size()).toBe(2)

  let bagI = tBag.all()
  let tmp = bagI.next()

  expect(tmp.value).toEqual(expect.stringMatching(/B|t|1|2/))
  expect(bagI.next().value).toEqual(expect.stringMatching(/B|t|1|2/))
  tBag.add('1')
  tBag.add('2')
  
  expect(tBag.isEmpty()).toBe(false)
  expect(tBag.size()).toBe(4)

  bagI = tBag.all()
  expect(bagI.next().value).toEqual(expect.stringMatching(/B|t|1|2/))
  expect(bagI.next().value).toEqual(expect.stringMatching(/B|t|1|2/))
  expect(bagI.next().value).toEqual(expect.stringMatching(/B|t|1|2/))
  expect(bagI.next().value).toEqual(expect.stringMatching(/B|t|1|2/))
  expect(bagI.next().done).toBe(true)
})

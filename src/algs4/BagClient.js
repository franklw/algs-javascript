var Bag = require('./Bag')

const tBag = new Bag()
tBag.add('s')
tBag.add('t')
console.log('isEmpty', tBag.isEmpty())
console.log('size', tBag.size())
tBag.add(1)
tBag.add(2)
console.log('isEmpty', tBag.isEmpty())
console.log('size', tBag.size())

for (const i of tBag.getAll()) {
  console.log(i)
}

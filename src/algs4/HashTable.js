/* eslint-disable space-before-function-paren  */

class Node {
  constructor (k, v) {
    this.key = k
    this.val = v
    this.next = null
  }
}

class HashTable {
  constructor () {
    // this.m = 997;
    this.m = 17
    this.st = new Array(this.m)
    this.cashedHash = {}
  }

  hashString(s) {
    let hash = 0
    let i
    let chr

    if (s.length === 0) return hash

    for (i = 0; i < s.length; i++) {
      chr = s.charCodeAt(i)
      hash = ((hash << 5) - hash) + chr
      hash |= 0 // convert to 32bit int
    }

    return hash
  }

  hashCode(j) {
    if (this.cashedHash.hasOwnProperty(j))
      return this.cashedHash[j]
    if (typeof j === 'string') {
      this.cashedHash[j] = this.hashString(j)
      return this.cashedHash[j]
    } else {
      this.cashedHash[j] = j % this.m
      return this.cashedHash[j]
    }
  }

  add(k, v) {
    const h = this.hashCode(k) % this.m
    if (this.st[h]) {
      let n = this.st[h]
      while (n.val !== v && n.next !== null) n = n.next

      if (n.val !== v) n.next = new Node(k, v)
    } else {
      this.st[h] = new Node(k, v)
    }
  }

  get(k) {
    const h = this.hashCode(k) % this.m
    if (this.st[h]) {
      let n = this.st[h]
      while (n.key !== k) n = n.next
      if (n === null)
        return null
      else
        return { keyHash: h, val: n.val }
    }
  }

  del(k, r = false) {
    const h = this.hashCode(k) % this.m
    if (this.st[h]) {
      let last = null
      let n = this.st[h]
      while (n.key !== k) {
        last = n
        n = n.next
      }
      if (n !== null) {
        const v = n.val
        if (n.next) {
          last.next = n.next // remove from list
        } else {
          last.next = null // end list
        }
        if (r)
          return v
      }
    }
  }
}

export default HashTable

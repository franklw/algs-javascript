
/**
 *
 * @param {*} a
 * @param {*} dirc
 * @param {*} compF
 */

const mergeSort = (a, dirc = 'ASC', compF = null) => {
  // divide and conquer
  // Move recuseively into array until there are two arrays of on item.
  // (Which means two pointers, pointing to the first two items)
  // Merge the two arrays (which start at length 1), in sort order.
  // This will result in two arrays of two items, where each array is sorted.
  // Merge these two arrays, positing the items in order at the merge id done.

  // Use Space of O(2N), aux array to hold items in process. 
  // Time O(nlogn) (since we are dividing the array in half in each recursion)

  const aux = []
  let bD = true
  if (dirc !== 'ASC') bD = false

  const sort = (a, lo, hi) => {
    if (hi <= lo) return

    const mid = lo + Math.floor((hi - lo) / 2)

    sort(a, lo, mid) // recuresively reduce indeices down to arrays of one item
    sort(a, mid + 1, hi)

    merge(a, lo, mid, hi) // merge two arrays into a new array, merge will position items in order as merge happens.
  }

  const merge = (a, lo, mid, hi) => {
    let left = lo
    let right = mid + 1

    for (let k = lo; k <= hi; k++) aux[k] = a[k]

    for (let dest = lo; dest <= hi; dest++) {
      if (left > mid)
        // boundry condition left pointer has moved past mid pointer
        // There are no more items in the laft hand array
        a[dest] = aux[right++]
      else if (right > hi)
        // boundry condition. right pointer has moved past hi pointer
        // there are no more items in the right had array
        a[dest] = aux[left++]

      else if (aux[right] < aux[left])
        // if right hand item is less than left, write the right hand items first
        a[dest] = aux[right++]

      else
        // else write the lefhand item first
        a[dest] = aux[left++]
    }
  }

  const less = (a, b) => {
    if (compare === null)
      return a < b
    else
      return compare(a, b)
  }

  sort(a, 0, a.length - 1)
  if (!bD) a = a.reverse()
}

module.exports = mergeSort

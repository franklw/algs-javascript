class Stack {
  constructor () {
    this.items = new Array(4)
    this.ct = 0
  }

  // Public
  push (v) {
    if (this.ct + 1 > this.items.length) this.expandItems()
    this.items[this.ct++] = v
  }

  pop () {
    if (this.ct - 1 < this.items.length / 2) this.reduceItems()
    const temp = this.items[this.ct - 1]
    this.items[this.ct--] = null
    return temp
  }

  isEmpty () {
    return this.ct === 0
  }

  size () {
    return this.ct
  }

  // Private
  expandItems () {
    this.items = [...this.items, ...new Array(this.items.length)]
  }

  reduceItems () {
    if ((this.items.length / 2) > 4) this.items = this.items.slice(0, this.items.length / 2)
  }
}

module.exports = Stack

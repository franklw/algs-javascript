
import { exch } from './Helpers'

/**
 * 
 * @param {*} a 
 * @param {*} dirc 
 */
const insertSort = (a, dirc = 'ASC') => {
  let bD = true
  if (dirc !== 'ASC') bD = false
  const n = a.length

  for (let endOfSorted = 1; endOfSorted < n; endOfSorted++) {
    for (let testSorted = endOfSorted; testSorted > 0 && a[testSorted] < a[testSorted - 1]; testSorted--)
      exch(a, testSorted, testSorted - 1)
  }
  if (!bD) a = a.reverse()
}

export default insertSort

/* eslint-disable no-extend-native */

import {exch } from './Helpers'

Array.prototype.selectSort = function (dirc = 'ASC') {
  const n = this.length
  let bD = true
  if (dirc !== 'ASC') bD = false

  for (let i = 0; i < n; i++) {
    let min = i
    for (let j = i + 1; j < n; j++) {
      if (this[j] < this[min]) min = j
    }
    exch(this, i, min)
  }
  if (!bD) this.reverse()
}

/* global test, expect */

import HashTable from './HashTable'

test('test hashTable', () => {

  const sH = new HashTable()
  const s = 'To be or not to be'
  const sA = s.split(' ')
  sA.forEach(w => {
    sH.add(w, w)
  })

  // console.log(sH.get('to'))
  expect(sH.get('to')).toStrictEqual({ keyHash: 1, val: 'to' })

})
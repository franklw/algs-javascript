import NodeItem from './NodeItem'

/**
 *
 *
 * @class Queue
 */
class Queue {
  constructor () {
    this.head = null
    this.tail = null
    this.ct = 0
  }

  // Public
  enqueue (v) {
    const newItem = new NodeItem(v)
    if (this.head === null) this.head = newItem
    if (this.tail === null) this.tail = newItem
    else {
      this.tail.next = newItem
      this.tail = newItem
    }
    this.ct++
  }

  dequeue () {
    let temp = null
    if (this.head !== null) {
      temp = this.head.val
      if (this.head.next === this.tail) this.tail = null
      this.head = this.head.next
      this.ct--
    }
    return temp
  }

  isEmpty () {
    return this.ct === 0
  }

  size () {
    return this.ct
  }

  peek () {
    return this.head.val
  }
}

module.exports = Queue
